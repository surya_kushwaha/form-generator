package auriga.form.generator;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by surya on 21/12/16.
 */

public final class MapFormModelV1 extends FormModel {
  private final Map<String, Object> data = new HashMap<>();

  @Override public Object getBackingValue(String name) {
    return data.get(name);
  }

  @Override public void setBackingValue(String name, Object value) {
    data.put(name, value);
  }
}
