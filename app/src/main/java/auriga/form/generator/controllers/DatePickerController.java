package auriga.form.generator.controllers;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import auriga.form.generator.FormController;
import auriga.form.generator.validations.InputValidator;

/**
 * Created by surya on 8/9/16.
 */
public class DatePickerController extends LabeledFieldController
{
    private final int editTextId = FormController.generateViewId();

    private DatePickerDialog datePickerDialog = null;
    private final SimpleDateFormat displayFormat;
    private final TimeZone timeZone;

    /**
     * Constructs a new instance of a date picker field.
     */
    public DatePickerController(Context ctx, String name, String labelText, Set<InputValidator> validators, SimpleDateFormat displayFormat)
    {
        super(ctx, name, labelText, validators);
        this.displayFormat = displayFormat;
        this.timeZone = displayFormat.getTimeZone();
    }

    /**
     * Constructs a new instance of a date picker field.
     */
    public DatePickerController(Context ctx, String name, String labelText, boolean isRequired, SimpleDateFormat displayFormat)
    {
        super(ctx, name, labelText, isRequired);
        this.displayFormat = displayFormat;
        this.timeZone = displayFormat.getTimeZone();
    }

    /**
     * Constructs a new instance of a date picker field, with the selected date displayed in "MMM d, yyyy" format.
     *
     * @param name
     *         the name of the field
     * @param labelText
     *         the label to display beside the field
     */
    public DatePickerController(Context context, String name, String labelText)
    {
        this(context, name, labelText, false, new SimpleDateFormat("MMM d, yyyy", Locale.getDefault()));
    }

    @Override
    protected View createFieldView()
    {
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        TextInputLayout titleWrapper = new TextInputLayout(getContext());
        titleWrapper.setLayoutParams(layoutParams);
        if(isRequired())
        {
            titleWrapper.setHint(getLabel() + "");
        }
        else
        {
            titleWrapper.setHint(getLabel());
        }

        final EditText editText = new EditText(getContext());
        editText.setId(editTextId);

        editText.setSingleLine(true);
        editText.setInputType(InputType.TYPE_CLASS_DATETIME);
        editText.setKeyListener(null);
        refresh(editText);
        editText.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showDatePickerDialog(getContext(), editText);
            }
        });

        editText.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(hasFocus)
                {
                    showDatePickerDialog(getContext(), editText);
                }
            }
        });
        titleWrapper.addView(editText);
        return titleWrapper;
    }

    private void showDatePickerDialog(final Context context, final EditText editText)
    {
        // don't show dialog again if it's already being shown
        if(datePickerDialog == null)
        {
            Date date = (Date) getModel().getValue(getName());
            if(date == null)
            {
                date = new Date();
            }
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTimeZone(timeZone);
            calendar.setTime(date);

            datePickerDialog = new DatePickerDialog(context, new OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                {
                    Calendar calendar = Calendar.getInstance(Locale.getDefault());
                    calendar.setTimeZone(timeZone);
                    calendar.set(year, monthOfYear, dayOfMonth);

                    Date formatted = null;
                    try
                    {
                        formatted = displayFormat.parse(displayFormat.format(calendar.getTime()));
                    }
                    catch(ParseException e)
                    {
                        e.printStackTrace();
                    }
                    editText.setText(displayFormat.format(calendar.getTime()));
                    getModel().setValue(getName(), displayFormat.format(calendar.getTime()));
                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

            datePickerDialog.setOnDismissListener(new OnDismissListener()
            {
                @Override
                public void onDismiss(DialogInterface dialog)
                {
                    datePickerDialog = null;
                }
            });

            datePickerDialog.show();
        }
    }

    private EditText getEditText()
    {
        return (EditText) getView().findViewById(editTextId);
    }

    private void refresh(EditText editText)
    {
        String value = (String) getModel().getValue(getName());
        editText.setText(value != null ? value : "");
    }

    public void refresh()
    {
        refresh(getEditText());
    }
}
