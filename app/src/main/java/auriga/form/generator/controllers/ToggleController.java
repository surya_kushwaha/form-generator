package auriga.form.generator.controllers;

import android.content.Context;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

import java.util.Set;

import auriga.form.generator.FormController;
import auriga.form.generator.validations.InputValidator;
/**
 * Created by surya on 8/9/16.
 */
public class ToggleController extends LabeledFieldController
{
    private final int toggleId = FormController.generateViewId();
    Switch aSwitch;

    public ToggleController(Context ctx, String name, String labelText, boolean isRequired)
    {
        super(ctx, name, labelText, isRequired);
    }

    public ToggleController(Context ctx, String name, String labelText, Set<InputValidator> validators)
    {
        super(ctx, name, labelText, validators);
    }

    @Override
    protected View createFieldView()
    {
        aSwitch = new Switch(getContext());
        aSwitch.setTextOn("ON");
        aSwitch.setTextOff("OFF");
        aSwitch.setChecked(true);
        if(isRequired()){
            aSwitch.setText(getLabel()+"*");
        }else{
            aSwitch.setText(getLabel());
        }

        aSwitch.setTextSize(16);
                aSwitch.setLayoutParams(
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {

                getModel().setValue(getName(), isChecked);
            }
        });
        if(aSwitch.isChecked())
        {
            getModel().setValue(getName(), true);
        }
        else
        {

            getModel().setValue(getName(), false);
        }

        return aSwitch;
    }

    @Override
    public void refresh()
    {
        if(aSwitch.isChecked())
        {
            getModel().setValue(getName(), true);
        }
        else
        {
            getModel().setValue(getName(), false);
        }
    }
}
