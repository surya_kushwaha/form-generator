package auriga.form.generator.controllers;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import auriga.form.generator.FormController;
import auriga.form.generator.R;
import auriga.form.generator.validations.InputValidator;
import java.util.List;
import java.util.Set;

/**
 * Created by surya on 8/9/16.
 */
public class SelectionController extends LabeledFieldController
{

    private final int spinnerId = FormController.generateViewId();

    private final String prompt;
    private final List<String> items;
    private final List<?> values;

    /**
     * Constructs a selection field
     */
    public SelectionController(Context ctx, String name, String labelText, Set<InputValidator> validators, String prompt,
                               List<String> items, boolean useItemsAsValues)
    {
        this(ctx, name, labelText, validators, prompt, items, useItemsAsValues ? items : null);
    }

    /**
     * Constructs a selection field
     *
     * @param ctx
     *         the Android context
     */
    public SelectionController(Context ctx, String name, String labelText, Set<InputValidator> validators, String prompt,
                               List<String> items, List<?> values)
    {
        super(ctx, name, labelText, validators);
        this.prompt = prompt;
        this.items = items;
        this.values = values;
    }

    /**
     * Constructs a selection field
     */
    public SelectionController(Context ctx, String name, String labelText, boolean isRequired, String prompt, List<String> items,
                               boolean useItemsAsValues)
    {
        this(ctx, name, labelText, isRequired, prompt, items, useItemsAsValues ? items : null);
    }

    /**
     * Constructs a selection field
     */
    public SelectionController(Context ctx, String name, String labelText, boolean isRequired, String prompt, List<String> items,
                               List<?> values)
    {
        super(ctx, name, labelText, isRequired);
        this.prompt = prompt;
        this.items = items;
        this.values = values;
    }

    /**
     * Returns the Spinner view associated with this element.
     *
     * @return the Spinner view associated with this element
     */
    public Spinner getSpinner()
    {
        return (Spinner) getView().findViewById(spinnerId);
    }

    @Override
    protected View createFieldView()
    {
        String hintText = null;
        Spinner spinnerView = new Spinner(getContext());
        spinnerView.setId(spinnerId);
        spinnerView.setPrompt(prompt);
        if(isRequired())
        {
            hintText = prompt + "*";
        }
        else
        {
            hintText = prompt;
        }
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, items);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerView.setAdapter(new NothingSelectedSpinnerAdapter(spinnerAdapter, R.layout.nothing_selected, getContext(), hintText));
        spinnerView.setOnItemSelectedListener(new OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
            {
                Object value;
                // if no values are specified, set the index on the model
                if(values == null)
                {
                    value = pos;
                }
                else
                {
                    // pos of 0 indicates nothing is selected
                    if(pos == 0)
                    {
                        value = null;
                    }
                    else
                    {    // if something is selected, set the value on the model
                        value = values.get(pos - 1);
                    }
                }

                getModel().setValue(getName(), value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });

        refresh(spinnerView);

        return spinnerView;
    }

    private void refresh(Spinner spinner)
    {
        Object value = getModel().getValue(getName());
        int selectionIndex = 0;

        if(values != null)
        {
            for(int i = 0; i < values.size(); i++)
            {
                if(values.get(i).equals(value))
                {
                    selectionIndex = i + 1;
                    break;
                }
            }
        }
        else if(value instanceof Integer)
        {
            selectionIndex = (Integer) value;
        }

        spinner.setSelection(selectionIndex);
    }

    @Override
    public void refresh()
    {
        refresh(getSpinner());
    }
}
