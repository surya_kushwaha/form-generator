package auriga.form.generator.controllers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import auriga.form.generator.R;

/**
 * Created by surya on 8/9/16.
 */
public class ValueController extends LabeledFieldController {

    /**
     * Constructs a new instance of a value field.
     *

     */
    public ValueController(Context ctx, String name, String labelText) {
        super(ctx, name, labelText, false);
    }

    @Override
    protected View createFieldView() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        final TextView textView = (TextView)layoutInflater.inflate(R.layout.value_field, null);
        refresh(textView);

        return textView;
    }

    private TextView getTextView() {
        return (TextView)getView().findViewById(R.id.value_text);
    }

    private void refresh(TextView textView) {
        Object value = getModel().getValue(getName());
        textView.setText(value != null ? value.toString() : "");
    }

    public void refresh() {
        refresh(getTextView());
    }
}