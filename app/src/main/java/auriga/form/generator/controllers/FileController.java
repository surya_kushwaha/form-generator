package auriga.form.generator.controllers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import auriga.form.generator.R;
import auriga.form.generator.app.SessionManager;
import auriga.form.generator.validations.InputValidator;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by surya on 8/9/16.
 */
public class FileController extends LabeledFieldController {
  private static final int REQUEST_WRITE_PERMISSION = 1;
  private Uri fileUri; // file url to store image/video
  private static final String IMAGE_DIRECTORY_NAME = "Kapila Images";
  public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
  private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
  public static final int MEDIA_TYPE_IMAGE = 1;
  public static final int MEDIA_TYPE_VIDEO = 2;
  SessionManager session;
  Context mContext;

  public FileController(Context ctx, String name, String labelText, boolean isRequired) {
    super(ctx, name, labelText, isRequired);
    mContext = ctx;
    session = new SessionManager(getContext().getApplicationContext());
  }

  public FileController(Context ctx, String name, String labelText,
      Set<InputValidator> validators) {
    super(ctx, name, labelText, validators);
    session = new SessionManager(getContext().getApplicationContext());
  }

  @Override protected View createFieldView() {
    LayoutInflater inflater =
        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.abs_file, null);
    TextView title = (TextView) view.findViewById(R.id.txtFileTitle);
    if (isRequired()) {
      title.setText(getLabel() + "*");
    } else {
      title.setText(getLabel());
    }

    Button takePicture = (Button) view.findViewById(R.id.buttonTakePicture);
    final ImageView picture = (ImageView) view.findViewById(R.id.ivClickPicture);
    final ImageView closePicture = (ImageView) view.findViewById(R.id.ivClosePicture);
    closePicture.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        picture.setVisibility(View.GONE);
        closePicture.setVisibility(View.GONE);
        getModel().setValue(getName(), null);
      }
    });
    takePicture.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (isDeviceSupportCamera()) {
          //   Common.L(""+getName());
          session.createAndUpdateControllerKey(getName());
          // Common.L(""+session.getKeyController());
          if (ActivityCompat.checkSelfPermission(getContext(), WRITE_EXTERNAL_STORAGE)
              == PackageManager.PERMISSION_GRANTED) {
            captureImage();
          } else {
            new AlertDialog.Builder(getContext()).setTitle(R.string.alert_title)
                .setMessage(R.string.alert_permission_not_grant)
                .setPositiveButton(R.string.change_permission,
                    new DialogInterface.OnClickListener() {

                      @Override @TargetApi(Build.VERSION_CODES.M)
                      public void onClick(DialogInterface dialog, int which) {

                        ActivityCompat.requestPermissions((Activity) mContext,
                            new String[] { WRITE_EXTERNAL_STORAGE }, REQUEST_WRITE_PERMISSION);
                        dialog.dismiss();
                      }
                    })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                  @Override public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                  }
                })
                .show();
          }
        }
      }
    });
    return view;
  }

  @Override public void refresh() {
    //  refresh(getImageView());
  }

  public ImageView getImageView() {
    return (ImageView) getView().findViewById(R.id.ivClickPicture);
  }

  public ImageView getCloseButton() {
    return (ImageView) getView().findViewById(R.id.ivClosePicture);
  }

  /**
   * Checking device has camera hardware or not
   */
  private boolean isDeviceSupportCamera() {
    if (getContext().getApplicationContext()
        .getPackageManager()
        .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
      // this device has a camera
      return true;
    } else {
      // no camera on this device
      return false;
    }
  }

  /*
* Capturing Camera Image will lauch camera app requrest image capture
*/
       /*
     * returning image / video
     */
  private static File getOutputMediaFile(int type) {

    // External sdcard location
    File mediaStorageDir =
        new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            IMAGE_DIRECTORY_NAME);

    // Create the storage directory if it does not exist
    if (!mediaStorageDir.exists()) {
      if (!mediaStorageDir.mkdirs()) {
        Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create " + IMAGE_DIRECTORY_NAME + " directory");
        return null;
      }
    }

    // Create a media file name
    String timeStamp =
        new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
    File mediaFile;
    if (type == MEDIA_TYPE_IMAGE) {
      mediaFile =
          new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
    } else if (type == MEDIA_TYPE_VIDEO) {
      mediaFile =
          new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
    } else {
      return null;
    }

    return mediaFile;
  }

  private void captureImage() {
    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

    getModel().setValue(getName(), fileUri);
    // start the image capture Intent
    ((Activity) getContext()).startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
  }

  /**
   * Creating file uri to store image/video
   */
  public Uri getOutputMediaFileUri(int type) {
    return Uri.fromFile(getOutputMediaFile(type));
  }
}
