package auriga.form.generator.controllers;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.Set;

import auriga.form.generator.FormController;
import auriga.form.generator.validations.InputValidator;
/**
 * Created by surya on 21/9/16.
 */
public class NonEditableTextController extends LabeledFieldController
{
    private final int editTextId = FormController.generateViewId();

    private int inputType;
    private final String placeholder;

    /**
     * Constructs a new instance of an edit text field.
     */
    public NonEditableTextController(Context ctx, String name, String labelText, String placeholder, Set<InputValidator> validators,
                                     int inputType)
    {
        super(ctx, name, labelText, validators);
        this.placeholder = placeholder;
        this.inputType = inputType;
    }

    /**
     * Constructs a new instance of an edit text field.
     */
    public NonEditableTextController(Context ctx, String name, String labelText, String placeholder, Set<InputValidator> validators)
    {
        this(ctx, name, labelText, placeholder, validators, InputType.TYPE_CLASS_TEXT);
    }

    /**
     * Constructs a new instance of an edit text field.
     */
    public NonEditableTextController(Context ctx, String name, String labelText, String placeholder, boolean isRequired, int inputType)
    {
        super(ctx, name, labelText, isRequired);
        this.placeholder = placeholder;
        this.inputType = inputType;
    }

    /**
     * Constructs a new instance of an edit text field.
     */
    public NonEditableTextController(Context ctx, String name, String labelText, String placeholder, boolean isRequired)
    {
        this(ctx, name, labelText, placeholder, isRequired, InputType.TYPE_CLASS_TEXT);
    }

    /**
     * Constructs a new instance of an edit text field.
     */
    public NonEditableTextController(Context ctx, String name, String labelText, String placeholder)
    {
        this(ctx, name, labelText, placeholder, false, InputType.TYPE_CLASS_TEXT);
    }

    /**
     * Constructs a new instance of an edit text field.
     *
     * @param ctx
     *         the Android context
     * @param name
     *         the name of the field
     * @param labelText
     *         the label to display beside the field
     */
    public NonEditableTextController(Context ctx, String name, String labelText)
    {
        this(ctx, name, labelText, null, false, InputType.TYPE_CLASS_TEXT);
    }

    /**
     * Returns the EditText view associated with this element.
     *
     * @return the EditText view associated with this element
     */
    public EditText getEditText()
    {
        return (EditText) getView().findViewById(editTextId);
    }

    /**
     * Returns a mask representing the content input type. Possible values are defined by {@link InputType}.
     *
     * @return a mask representing the content input type
     */
    public int getInputType()
    {
        return inputType;
    }

    private void setInputTypeMask(int mask, boolean enabled)
    {
        if(enabled)
        {
            inputType = inputType | mask;
        }
        else
        {
            inputType = inputType & ~mask;
        }
        if(isViewCreated())
        {
            getEditText().setInputType(inputType);
        }
    }

    /**
     * Indicates whether this text box has multi-line enabled.
     *
     * @return true if this text box has multi-line enabled, or false otherwise
     */
    public boolean isMultiLine()
    {
        return (inputType | InputType.TYPE_TEXT_FLAG_MULTI_LINE) != 0;
    }

    /**
     * Enables or disables multi-line input for the text field. Default is false.
     *
     * @param multiLine
     *         if true, multi-line input is allowed, otherwise, the field will only allow a single line.
     */
    public void setMultiLine(boolean multiLine)
    {
        setInputTypeMask(InputType.TYPE_TEXT_FLAG_MULTI_LINE, multiLine);
    }

    /**
     * Indicates whether this text field hides the input text for security reasons.
     *
     * @return true if this text field hides the input text, or false otherwise
     */
    public boolean isSecureEntry()
    {
        return (inputType | InputType.TYPE_TEXT_VARIATION_PASSWORD) != 0;
    }

    /**
     * Enables or disables secure entry for this text field. If enabled, input will be hidden from the user. Default is
     * false.
     *
     * @param isSecureEntry
     *         if true, input will be hidden from the user, otherwise input will be visible.
     */
    public void setSecureEntry(boolean isSecureEntry)
    {
        setInputTypeMask(InputType.TYPE_TEXT_VARIATION_PASSWORD, isSecureEntry);
    }

    @Override
    protected View createFieldView()
    {
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        TextInputLayout titleWrapper = new TextInputLayout(getContext());
        titleWrapper.setLayoutParams(layoutParams);
        titleWrapper.setHint(getLabel());
        final EditText editText = new EditText(getContext());
        editText.setId(editTextId);
        editText.setSingleLine(!isMultiLine());
        editText.setClickable(false);
        editText.setCursorVisible(false);
        titleWrapper.setFocusable(false);
        titleWrapper.addView(editText);
        editText.setHint(getLabel());
        editText.setText(placeholder + " ");

        return titleWrapper;
    }

    private void refresh(EditText editText)
    {
        Object value = getModel().getValue(getName());
        String valueStr = value != null ? value.toString() : "";
        if(!valueStr.equals(editText.getText().toString()))
        {
            editText.setText(valueStr);
        }
    }

    @Override
    public void refresh()
    {

    }
}
