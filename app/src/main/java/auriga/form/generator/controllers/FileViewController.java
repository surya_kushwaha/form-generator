package auriga.form.generator.controllers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import auriga.form.generator.R;
import com.squareup.picasso.Picasso;

/**
 * Created by surya on 19/9/16.
 */
public class FileViewController extends LabeledFieldController
{
    private String pictureUrl;

    public FileViewController(Context ctx, String name, String labelText, boolean isRequired)
    {
        super(ctx, name, labelText, isRequired);
    }

    public FileViewController(Context ctx, String name, String labelText, boolean isRequired, String pictureUrl)
    {
        super(ctx, name, labelText, isRequired);
        this.pictureUrl = pictureUrl;
    }

    @Override
    protected View createFieldView()
    {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.abs_file, null);
        TextView title = (TextView) view.findViewById(R.id.txtFileTitle);
        title.setText(getLabel());
        Button takePicture = (Button) view.findViewById(R.id.buttonTakePicture);
        final ImageView picture = (ImageView) view.findViewById(R.id.ivClickPicture);
        final ImageView closePicture = (ImageView) view.findViewById(R.id.ivClosePicture);
        takePicture.setVisibility(View.GONE);
        closePicture.setVisibility(View.GONE);
        picture.setVisibility(View.VISIBLE);
        if (pictureUrl != null && pictureUrl.length() > 4) {
            Picasso.with(getContext().getApplicationContext()).load(pictureUrl).into(picture);
        }

        return view;
    }

    @Override
    public void refresh()
    {

    }
}
