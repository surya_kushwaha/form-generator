package auriga.form.generator.controllers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import auriga.form.generator.R;
import auriga.form.generator.validations.InputValidator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by surya on 8/9/16.
 */
public class CheckBoxController extends LabeledFieldController {
  private final static int CHECKBOX_ID = 101010;
  private final List<String> items;
  private final List<?> values;

  /**
   * Constructs a new instance of a checkboxes field.
   */
  public CheckBoxController(Context ctx, String name, String labelText,
      Set<InputValidator> validators, List<String> items, boolean useItemsAsValues) {
    this(ctx, name, labelText, validators, items, useItemsAsValues ? items : null);
  }

  /**
   * Constructs a new instance of a checkboxes field.
   */
  public CheckBoxController(Context ctx, String name, String labelText,
      Set<InputValidator> validators, List<String> items, List<?> values) {
    super(ctx, name, labelText, validators);
    this.items = items;
    this.values = values;
  }

  /**
   * Constructs a new instance of a checkboxes field.
   */
  public CheckBoxController(Context ctx, String name, String labelText, boolean isRequired,
      List<String> items, boolean useItemsAsValues) {
    this(ctx, name, labelText, isRequired, items, useItemsAsValues ? items : null);
  }

  /**
   * Constructs a new instance of a checkboxes field.
   */
  public CheckBoxController(Context ctx, String name, String labelText, boolean isRequired,
      List<String> items, List<?> values) {
    super(ctx, name, labelText, isRequired);
    this.items = items;
    this.values = values;
  }

  @Override protected View createFieldView() {
    LayoutInflater inflater =
        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    ViewGroup checkboxContainer =
        (ViewGroup) inflater.inflate(R.layout.form_checkbox_container, null);

    CheckBox checkBox;
    int nbItem = items.size();
    for (int index = 0; index < nbItem; index++) {
      checkBox = new CheckBox(getContext());
      checkBox.setText(items.get(index));
      checkBox.setId(CHECKBOX_ID + index);
      checkBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
        @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
          int position = buttonView.getId() - CHECKBOX_ID;
          Object value = areValuesDefined() ? values.get(position) : position;
          Set<Object> modelValues = retrieveModelValues();
          if (isChecked) {
            modelValues.add(value);
          } else {
            modelValues.remove(value);
          }
          getModel().setValue(getName(), modelValues);
        }
      });

      checkboxContainer.addView(checkBox);
    }
    return checkboxContainer;
  }

  @Override public void refresh() {
    Set<Object> modelValues = retrieveModelValues();
    ViewGroup layout = getContainer();

    CheckBox checkbox;
    int nbItem = items.size();
    for (int index = 0; index < nbItem; index++) {
      checkbox = (CheckBox) layout.findViewById(CHECKBOX_ID + index);
      checkbox.setChecked(modelValues.contains(areValuesDefined() ? checkbox.getText() : index));
    }
  }

  /**
   * Returns the status of the values entry.
   *
   * @return true if values entry can be used. false otherwise.
   */
  private boolean areValuesDefined() {
    return values != null;
  }

  /**
   * Returns the values hold in the model.
   *
   * @return The values from the model.
   */
  private Set<Object> retrieveModelValues() {
    Set<Object> modelValues = (Set<Object>) getModel().getValue(getName());
    if (modelValues == null) {
      modelValues = new HashSet<>();
    }
    return modelValues;
  }

  /**
   * Returns the View containing the checkboxes.
   *
   * @return The View containing the checkboxes.
   */
  private ViewGroup getContainer() {
    return (ViewGroup) getView().findViewById(R.id.form_checkbox_container);
  }
}
