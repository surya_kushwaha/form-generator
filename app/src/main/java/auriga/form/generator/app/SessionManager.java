package auriga.form.generator.app;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by surya on 24/8/16.
 */
public class SessionManager {
  // Shared Preferences
  SharedPreferences pref;

  // Editor for Shared preferences
  SharedPreferences.Editor editor;

  // Context
  Context _context;

  // Shared pref mode
  //int PRIVATE_MODE = Context.MODE_PRIVATE;

  // Shared pref file name
  // private static final String PREF_NAME = "Jvvnl";
  private static final String PREF_NAME = "FormGenerator";

  // All Shared Preferences Keys
  private static final String KEY_CONTROLLER = "key_controller";



  // Constructor
  public SessionManager(Context context) {
    this._context = context;
    pref = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    editor = pref.edit();
  }


  /**
   * set controller key
   */

  public void createAndUpdateControllerKey(String value) {
    // Storing login value as TRUE
    editor.putString(KEY_CONTROLLER, value);
    //commit change
    editor.commit();
  }

  /**
   * get Controller key
   */
  public String getKeyController() {
    return pref.getString(KEY_CONTROLLER, null);
  }


}
