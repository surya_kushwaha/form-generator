package auriga.form.generator.validations;

public interface InputValidator {
    /**
     * Defines the validity of an object against a specific requirement.

     */
    ValidationError validate(Object value, String fieldName, String fieldLabel);
}
