package auriga.form.generator.validations;

import android.text.TextUtils;

public class RequiredFieldValidator implements InputValidator {
    @Override
    public ValidationError validate(Object value, String fieldName, String fieldLabel) {
        if (value == null || (value instanceof String && TextUtils.isEmpty((String) value))) {
            return new RequiredField(fieldName, fieldLabel);
        }
        return null;
    }

    /**
     * Makes every instances of {@link RequiredFieldValidator} equal.
     *

     */
    @Override
    public boolean equals(Object o) {
        return super.equals(o) || o != null && getClass() == o.getClass();
    }

    /**
     * Every instance of {{@link RequiredFieldValidator}} share the same hashcode.
     *
     * @return 0
     */
    @Override
    public int hashCode() {
        return 0;
    }
}
