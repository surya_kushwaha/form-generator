package auriga.form.generator;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Created by surya on 8/9/16.
 */
public abstract class FormModel extends Fragment
{
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    /**
     * @param name
     *         the field name to set the value for
     * @param newValue
     *         the value to set
     */
    protected abstract void setBackingValue(String name, Object newValue);

    /**
     * This method is called whenever the form needs the current value for a specific field. Subclasses must implement

     */
    protected abstract Object getBackingValue(String name);

    /**
     * Returns the value for the specified field name.

     */
    public final Object getValue(String name)
    {
        return getBackingValue(name);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    /**
     * Sets a value for the specified field name. A property change notification is fired to registered listeners if

     */
    public final void setValue(String name, Object newValue)
    {
        Object curValue = getBackingValue(name);
        if(!objectsEqual(curValue, newValue))
        {
            setBackingValue(name, newValue);
            propertyChangeSupport.firePropertyChange(name, curValue, newValue);
        }
    }

    private static boolean objectsEqual(Object a, Object b)
    {
        return a == b || (a != null && a.equals(b));
    }

    /**
     * Subscribes {@code listener} to change notifications for all fields.
     *
     * @see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     */
    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Subscribes {@code listener} to change notifications for the specified field name.
     *
     * @see PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     */
    public void addPropertyChangeListener(String fieldName, PropertyChangeListener listener)
    {
        propertyChangeSupport.addPropertyChangeListener(fieldName, listener);
    }

    /**
     * Unsubscribes {@code listener} from change notifications for all fields.
     *
     * @see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     */
    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    /**
     * Unsubscribes {@code listener} from change notifications for the specified field name.
     *
     * @see PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     */
    public void removePropertyChangeListener(String fieldName, PropertyChangeListener listener)
    {
        propertyChangeSupport.removePropertyChangeListener(fieldName, listener);
    }
}
