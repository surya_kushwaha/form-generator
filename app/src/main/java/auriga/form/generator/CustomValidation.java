package auriga.form.generator;

import android.content.res.Resources;

import auriga.form.generator.validations.InputValidator;
import auriga.form.generator.validations.ValidationError;

/**
 * Created by surya on 8/9/16.
 */
public class CustomValidation implements InputValidator
{
    @Override
    public ValidationError validate(Object value, String fieldName, String fieldLabel)
    {
        if(value != null)
        {
            if(value instanceof String)
            {
                try
                {
                    return assertMobile((String) value, fieldName, fieldLabel);
                }
                catch(NumberFormatException e)
                {
                    // Do nothing as the input isn't a number
                }
            }
            if(value instanceof Number)
            {
                return assertMobile((String) value, fieldName, fieldLabel);
            }
            // Any other type doesn't have to be checked by this validator
        }
        return null;
    }

    private ValidationError assertMobile(String value, String fieldName, String fieldLabel)
    {
        if(value.length()!=10)
        {
            return new ValidationError(fieldName, fieldLabel)
            {
                @Override
                public String getMessage(Resources resources)
                {
                    return resources.getString(R.string.validation_error_title);
                }
            };
        }
        return null;
    }
}
